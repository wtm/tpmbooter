use crate::shaders;
use tpmbooter_resources::LEFT_PTR;
use crate::gfx_components::*;

pub struct Cursor {
  pub drawer: simple_image::SimpleImage<Texture>,
}

impl Cursor {
  pub const SCREEN_SIZE: f32 = 96.0 / 4.0;
  pub const X_OFFSET: f32 = -12.0 * Cursor::SCREEN_SIZE / 96.0;
  pub const Y_OFFSET: f32 = -8.0 * Cursor::SCREEN_SIZE / 96.0;

  pub fn init(_init_ctx: &InitContext) -> Result<Self, String> {
    let tex = Texture::from_buildtime_image(&LEFT_PTR).map_err(|x| format!("opengl error: {}", x))?;
    tex.bind();
    unsafe {
      gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR_MIPMAP_NEAREST as i32);
    }
    Ok(Self{
      drawer: simple_image::SimpleImage::init_from_texture(tex, 0f32, 0f32)?
    })
  }
}

impl Drawable for Cursor {
  fn draw(&mut self, draw_ctx: &mut DrawContext) {
    let (cursor_x, cursor_y) = draw_ctx.cursor_pos();
    self.drawer.bounding_box = (cursor_x as f32 + Self::X_OFFSET, cursor_y as f32 + Self::Y_OFFSET, Self::SCREEN_SIZE, Self::SCREEN_SIZE);
    self.drawer.draw(draw_ctx);
  }
}
