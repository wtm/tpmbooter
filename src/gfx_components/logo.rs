use crate::gfx_components::*;
use tpmbooter_resources::ARCHLINUX;
use std::time;

pub struct Logo {
  fader: transitions::BoxFader,
  fader_data: transitions::DrawData<Texture>,
  width: f32,
  height: f32,
}

impl Logo {
  pub fn init(init_ctx: &InitContext) -> Result<Self, String> {
    let mut fader = transitions::BoxFader::init_box(init_ctx)?;
    let tex = Texture::from_buildtime_image(&ARCHLINUX).map_err(|x| format!("{}", x))?;
    tex.bind();
    unsafe {
      gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR_MIPMAP_NEAREST as i32);
      gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as i32);
    }
    let width = init_ctx.screen_width() as f32 / 4.0;
    let height = width * (tex.height as f32 / tex.width as f32);
    let x = init_ctx.screen_width() as f32 / 2.0 - width / 2.0;
    let y = init_ctx.screen_height() as f32 / 2.0 - height / 2.0;
    let fader_data = transitions::DrawData{texture: tex, bounding_box: (x, y, width, height)};
    fader.start_transition(init_ctx.now, time::Duration::SECOND);
    Ok(Self{
      fader, fader_data, width, height
    })
  }
}

impl Drawable for Logo {
  fn draw(&mut self, draw_ctx: &mut DrawContext) {
    self.fader.draw_box(draw_ctx, None, Some(self.fader_data.as_ref()));
  }
}
