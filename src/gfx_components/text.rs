use crate::gfx_components::*;
use rusttype;
use std::{mem, time};

pub struct TextRenderer<'a> {
  pub scale: rusttype::Scale,
  pub font: rusttype::Font<'a>,
  pub color: (f32, f32, f32, f32),
  baseline_y: f32,
  texture_height: f32,
}

impl<'a> TextRenderer<'a> {
  pub fn init(font: rusttype::Font<'a>) -> Self {
    let mut this = Self{
      font,
      scale: rusttype::Scale::uniform(16.0),
      color: (1.0, 1.0, 1.0, 1.0),
      baseline_y: 0f32,
      texture_height: 0f32,
    };
    this.set_scale(16.0);
    this
  }

  pub fn set_font(&mut self, font: rusttype::Font<'a>) {
    self.font = font;
    self.set_scale(self.scale.y);
  }
  pub fn set_scale(&mut self, scale: f32) {
    self.scale = rusttype::Scale::uniform(scale);
    let vmetrics = self.font.v_metrics(self.scale);
    self.baseline_y = vmetrics.ascent;
    self.texture_height = vmetrics.ascent + f32::abs(vmetrics.descent);
  }
  pub fn set_color(&mut self, color: (f32, f32, f32, f32)) {
    self.color = color;
  }
  pub fn get_baseline_y_offset(&self) -> f32 {
    self.baseline_y
  }

  pub fn get_text_texture(&self, text: &str) -> Option<Texture> {
    self.get_text_texture_oversampled(text, 1)
  }

  pub fn get_text_texture_oversampled(&self, text: &str, factor: u32) -> Option<Texture> {
    if factor == 0 {
      panic!("!");
    }
    if text.len() == 0 {
      return None;
    }
    let layout: Vec<_> = self.font.layout(text, rusttype::Scale{x: self.scale.x * factor as f32, y: self.scale.y * factor as f32}, rusttype::point(0.0, self.baseline_y * factor as f32)).collect();
    if layout.len() == 0 {
      return None;
    }
    let last_gly = layout.last().unwrap();
    let pix_width = (last_gly.position().x + last_gly.unpositioned().h_metrics().advance_width).ceil() as u32;
    let pix_height = self.texture_height as u32 * factor;
    let mut image_data = vec![0f32; (pix_width * pix_height) as usize * 4];
    for gly in layout.iter() {
      let bb = match gly.pixel_bounding_box() {
        Some(b) => b,
        None => continue,
      };
      gly.draw(|x, y, alpha| {
        let x = x as i32 + bb.min.x;
        let y = y as i32 + bb.min.y;
        if x >= 0 && x < pix_width as i32 && y >= 0 && y < pix_height as i32 {
          let offset = 4 * (y as u32 * pix_width + x as u32) as usize;
          image_data[offset] = self.color.0;
          image_data[offset + 1] = self.color.1;
          image_data[offset + 2] = self.color.2;
          image_data[offset + 3] = self.color.3 * alpha;
        }
      });
    }
    let tex = Texture::from_f32s(image_data.as_slice(), pix_width, pix_height).unwrap();
    Some(tex)
  }
}

pub enum Align {
  LEFT,
  RIGHT,
  CENTER
}

use transitions::FlowEffect;

pub struct FadeText<'a, T: std::borrow::Borrow<TextRenderer<'a>>> {
  _p: std::marker::PhantomData<&'a T>,
  pub text_renderer: T,
  fader: transitions::BoxFader,
  prev_draw_data: Option<transitions::DrawData<Texture>>,
  curr_draw_data: Option<transitions::DrawData<Texture>>,
  x: f32,
  y: f32,
  pub transition_duration: time::Duration,
  current_text: String,
  pub align: Align,
  pub flow_effect: FlowEffect,
  pub flow_effect_offset: f32,
  pub oversampling_factor: u32,
}

impl<'a, T: std::borrow::Borrow<TextRenderer<'a>>> FadeText<'a, T> {
  pub fn init(init_ctx: &InitContext, text: T, baseline_x: f32, baseline_y: f32) -> Result<Self, String> {
    Ok(Self{
      _p: std::marker::PhantomData,
      text_renderer: text,
      fader: transitions::BoxFader::init_box(init_ctx)?,
      prev_draw_data: None,
      curr_draw_data: None,
      x: baseline_x,
      y: baseline_y,
      transition_duration: time::Duration::from_millis(300),
      current_text: String::new(),
      align: Align::LEFT,
      flow_effect: FlowEffect::NONE,
      flow_effect_offset: 30f32,
      oversampling_factor: 1,
    })
  }

  pub fn set_origin(&mut self, new_x: f32, new_y: f32) {
    if let Some(ref mut d) = self.curr_draw_data {
      let curr_width = d.bounding_box.2;
      d.bounding_box.1 = new_y - self.text_renderer.borrow().get_baseline_y_offset();
      d.bounding_box.0 = match self.align {
        Align::LEFT => new_x,
        Align::CENTER => new_x - curr_width / 2.0,
        Align::RIGHT => new_x - curr_width,
      };
    }
    self.x = new_x;
    self.y = new_y;
  }

  pub fn update(&mut self, text: &str, now: time::Instant) {
    if &self.current_text == text {
      return;
    }
    let new_tex = self.text_renderer.borrow().get_text_texture_oversampled(text, self.oversampling_factor);
    if new_tex.is_none() {
      self.prev_draw_data = None;
      mem::swap(&mut self.curr_draw_data, &mut self.prev_draw_data);
      self.fader.start_transition(now, self.transition_duration);
      return;
    }
    let new_tex = new_tex.unwrap();
    let mut new_draw_data = transitions::DrawData::from_texture(new_tex, self.x, self.y);
    new_draw_data.bounding_box.2 /= self.oversampling_factor as f32;
    new_draw_data.bounding_box.3 /= self.oversampling_factor as f32;
    self.prev_draw_data = None;
    mem::swap(&mut self.curr_draw_data, &mut self.prev_draw_data);
    self.curr_draw_data = Some(new_draw_data);
    self.set_origin(self.x, self.y);
    self.fader.flow_effect = self.flow_effect;
    self.fader.flow_effect_offset = self.flow_effect_offset;
    self.fader.start_transition(now, self.transition_duration);
    self.current_text.clear();
    self.current_text.push_str(text);
  }

  fn set_tex_param(&self) {
    unsafe {
      if self.oversampling_factor == 1 {
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::NEAREST as i32);
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::NEAREST as i32);
      } else {
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as i32);
        if self.oversampling_factor == 2 {
          gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR as i32);
        } else {
          gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR_MIPMAP_NEAREST as i32);
        }
      }
    }
  }
}

impl<'a, T: std::borrow::Borrow<TextRenderer<'a>>> Drawable for FadeText<'a, T> {
  fn draw(&mut self, draw_ctx: &mut DrawContext) {
    if let Some(ref d) = self.prev_draw_data {
      d.texture.bind();
      self.set_tex_param();
    }
    if let Some(ref d) = self.curr_draw_data {
      d.texture.bind();
      self.set_tex_param();
    }
    self.fader.draw_box(draw_ctx, self.prev_draw_data.as_ref().map(|x| x.as_ref()), self.curr_draw_data.as_ref().map(|x| x.as_ref()));
  }
}
