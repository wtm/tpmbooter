use std::{time, mem};
use crate::shaders::*;

pub struct DrawContext<'a> {
  pub iptctx: &'a mut crate::inputs::LibinputCtx,
  pub gfx: &'a crate::gfx_init::GlIniter,
  pub now: time::Instant,
  pub stage_transform_matrix: &'a [f32],
}

pub trait Drawable {
  fn draw(&mut self, draw_ctx: &mut DrawContext);
}

impl<'a> DrawContext<'a> {
  pub fn cursor_pos(&self) -> (f64, f64) {
    (self.iptctx.cursor_x_norm * self.screen_width() as f64, self.iptctx.cursor_y_norm * self.screen_height() as f64)
  }
  pub fn screen_width(&self) -> i32 {
    self.gfx.width
  }
  pub fn screen_height(&self) -> i32 {
    self.gfx.height
  }
}

pub struct InitContext<'a> {
  pub gfx: &'a crate::gfx_init::GlIniter,
  pub stage_transform_matrix: &'a [f32],
  pub now: time::Instant,
}

impl<'a> InitContext<'a> {
  pub fn screen_width(&self) -> i32 {
    self.gfx.width
  }
  pub fn screen_height(&self) -> i32 {
    self.gfx.height
  }
}

pub mod cursor;
pub mod transitions;
pub mod text;
pub mod simple_image;
pub mod logo;

pub fn box_verticies(x: f32, y: f32, w: f32, h: f32) -> [f32; 8] {
  [x, y,
   x, y + h,
   x + w, y,
   x + w, y + h]
}

pub fn box_verticies_with_texcord(x: f32, y: f32, w: f32, h: f32) -> [f32; 16] {
  [x, y, 0f32, 0f32,
   x, y + h, 0f32, 1f32,
   x + w, y, 1f32, 0f32,
   x + w, y + h, 1f32, 1f32]
}

pub fn gen_box_vert_array_and_buf(gl_program: &Program) -> (u32, u32) {
  let mut vert_array = 0u32;
  let mut vert_buf = 0u32;
  unsafe {
    gl::GenVertexArrays(1, &mut vert_array);
    gl::BindVertexArray(vert_array);
    gl::GenBuffers(1, &mut vert_buf);
    gl::BindBuffer(gl::ARRAY_BUFFER, vert_buf);
    let ptr_coord = gl_program.get_attr_location(b"coordinate\0".as_ptr());
    let ptr_texcord = gl_program.get_attr_location(b"texcord_in\0".as_ptr());
    gl::VertexAttribPointer(ptr_coord, 2, gl::FLOAT, 0, (mem::size_of::<f32>() * 4) as i32, 0 as *mut _);
    gl::VertexAttribPointer(ptr_texcord, 2, gl::FLOAT, 0, (mem::size_of::<f32>() * 4) as i32, (2 * mem::size_of::<f32>()) as *mut _);
    gl::EnableVertexAttribArray(ptr_coord);
    gl::EnableVertexAttribArray(ptr_texcord);
  }
  (vert_array, vert_buf)
}

pub struct Texture {
  pub tex_id: u32,
  pub tex_type: u32,
  pub width: u32,
  pub height: u32,
}

impl Texture {
  pub fn from_buildtime_image(image: &tpmbooter_resources::Image) -> Result<Self, u32> {
    Self::from_data(image.data, image.width, image.height)
  }

  pub fn from_data(data: &[u8], width: u32, height: u32) -> Result<Self, u32> {
    unsafe {
      let mut tex_id: u32 = 0;
      let tex_type = gl::TEXTURE_2D;
      gl::GenTextures(1, &mut tex_id);
      gl::BindTexture(tex_type, tex_id);
      gl::TexImage2D(tex_type, 0, gl::RGBA as i32, width as i32, height as i32, 0, gl::RGBA, gl::UNSIGNED_BYTE, data.as_ptr() as *const _);
      gl::GenerateMipmap(tex_type);
      let err = gl::GetError();
      if err == gl::NO_ERROR {
        Ok(Self{tex_id, tex_type, width, height})
      } else {
        while gl::GetError() != gl::NO_ERROR {};
        Err(err)
      }
    }
  }

  pub fn from_f32s(data: &[f32], width: u32, height: u32) -> Result<Self, u32> {
    unsafe {
      let mut tex_id: u32 = 0;
      let tex_type = gl::TEXTURE_2D;
      gl::GenTextures(1, &mut tex_id);
      gl::BindTexture(tex_type, tex_id);
      gl::TexImage2D(tex_type, 0, gl::RGBA as i32, width as i32, height as i32, 0, gl::RGBA, gl::FLOAT, data.as_ptr() as *const _);
      gl::GenerateMipmap(tex_type);
      let err = gl::GetError();
      if err == gl::NO_ERROR {
        Ok(Self{tex_id, tex_type, width, height})
      } else {
        while gl::GetError() != gl::NO_ERROR {};
        Err(err)
      }
    }
  }

  pub fn bind(&self) {
    unsafe {
      gl::BindTexture(self.tex_type, self.tex_id);
    }
  }
}

impl Drop for Texture {
  fn drop(&mut self) {
    unsafe {
      gl::DeleteTextures(1, &self.tex_id);
    }
  }
}
