use crate::shaders::*;
use crate::gfx_components::*;
use gl;
use std::borrow::Borrow;

pub struct SimpleImage<T: Borrow<Texture>> {
  gl_program: Program,
  pub tex: Option<T>,
  pub bounding_box: (f32, f32, f32, f32),

  u_tex: i32,
  u_stage_transform: i32,
  vert_array: u32,
  vert_buf: u32,
}

impl<T: Borrow<Texture>> SimpleImage<T> {
  pub fn init(tex: Option<T>, x: f32, y: f32, w: f32, h: f32) -> Result<Self, String> {
    let vert = load_shader!("box_with_texcord.vert", gl::VERTEX_SHADER)?;
    let frag = load_shader!("box_with_texcord.frag", gl::FRAGMENT_SHADER)?;
    let gl_program = Program::link_shaders(&[vert, frag])?;
    gl_program.use_program();
    let (vert_array, vert_buf) = gen_box_vert_array_and_buf(&gl_program);
    unsafe {
      Ok(Self{
        tex,
        bounding_box: (x, y, w, h),
        u_tex: gl_program.get_uniform_location(b"tex\0".as_ptr()),
        u_stage_transform: gl_program.get_uniform_location(b"stageTransform\0".as_ptr()),
        gl_program,
        vert_array, vert_buf,
      })
    }
  }

  pub fn init_from_texture(tex: T, x: f32, y: f32) -> Result<Self, String> {
    let tex_ = tex.borrow();
    let w = tex_.width;
    let h = tex_.height;
    let mut this = Self::init(Some(tex), x, y, 0f32, 0f32)?;
    this.bounding_box.2 = w as f32;
    this.bounding_box.3 = h as f32;
    Ok(this)
  }
}

impl<T: Borrow<Texture>> Drawable for SimpleImage<T> {
  fn draw(&mut self, draw_ctx: &mut DrawContext) {
    if self.tex.is_none() { return; }
    let tex = self.tex.as_ref().unwrap().borrow();
    let (x, y, w, h) = self.bounding_box;
    let verts = box_verticies_with_texcord(x, y, w, h);
    self.gl_program.use_program();
    unsafe {
      gl::UniformMatrix3fv(self.u_stage_transform, 1, 0, draw_ctx.stage_transform_matrix.as_ptr());
      gl::Enable(gl::BLEND);
      gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);
      gl::BindVertexArray(self.vert_array);
      gl::BindBuffer(gl::ARRAY_BUFFER, self.vert_buf);
      gl::BufferData(gl::ARRAY_BUFFER, mem::size_of_val(&verts[..]) as isize, verts.as_ptr() as *const _, gl::DYNAMIC_DRAW);
      gl::ActiveTexture(gl::TEXTURE0);
      tex.bind();
      gl::Uniform1i(self.u_tex, 0);
			gl::DrawArrays(gl::TRIANGLE_STRIP, 0, 4);
    }
  }
}
