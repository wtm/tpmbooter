use crate::shaders::*;
use crate::gfx_components::*;
use std::time;

pub struct BoxFader {
  /// (start, duration, is_forward)
  transition: Option<(time::Instant, time::Duration)>,
  pub flow_effect: FlowEffect,
  pub flow_effect_offset: f32,

  gl_program: Program,
  u_texcord_prev_mat: i32,
  u_texcord_curr_mat: i32,
  u_tex_prev: i32,
  u_tex_curr: i32,
  u_prog: i32,
  u_has_prev: i32,
  u_has_curr: i32,
  u_stage_transform: i32,

  vert_array: u32,
  vert_buf: u32,
}

pub struct DrawData<T: std::borrow::Borrow<Texture>> {
  pub texture: T,
  /// (x, y, w, h)
  pub bounding_box: (f32, f32, f32, f32),
}

#[derive(PartialEq, Eq, Copy, Clone)]
pub enum FlowEffect {
  UP,
  DOWN,
  LEFT,
  RIGHT,
  NONE
}

impl<T: std::borrow::Borrow<Texture>> DrawData<T> {
  pub fn from_texture(texture: T, x: f32, y: f32) -> Self {
    let btext = texture.borrow();
    Self {
      bounding_box: (x, y, btext.width as f32, btext.height as f32),
      texture,
    }
  }
}

impl<T: std::borrow::Borrow<Texture> + Copy> std::clone::Clone for DrawData<T> {
  fn clone(&self) -> Self {
    Self{
      texture: self.texture,
      bounding_box: self.bounding_box,
    }
  }
}

impl DrawData<Texture> {
  pub fn as_ref(&self) -> DrawData<&Texture> {
    DrawData{
      texture: &self.texture,
      bounding_box: self.bounding_box,
    }
  }
}

impl BoxFader {
  pub fn init_box(init_ctx: &InitContext) -> Result<Self, String> {
    let vert = load_shader!("box_fader.vert", gl::VERTEX_SHADER)?;
    let frag = load_shader!("fader.frag", gl::FRAGMENT_SHADER)?;
    let gl_program = Program::link_shaders(&[vert, frag])?;
    gl_program.use_program();
    let mut vert_array = 0u32;
    let mut vert_buf = 0u32;
    unsafe {
      gl::GenVertexArrays(1, &mut vert_array);
      gl::BindVertexArray(vert_array);
      gl::GenBuffers(1, &mut vert_buf);
      gl::BindBuffer(gl::ARRAY_BUFFER, vert_buf);
      let ptr_coord = gl_program.get_attr_location(b"coordinate\0".as_ptr());
      gl::VertexAttribPointer(ptr_coord, 2, gl::FLOAT, 0, mem::size_of::<f32>() as i32 * 2, 0 as *mut _);
      gl::EnableVertexAttribArray(ptr_coord);
    }
    unsafe {
      Ok(BoxFader{
        transition: None,
        flow_effect: FlowEffect::NONE,
        flow_effect_offset: 30f32,
        u_texcord_prev_mat: gl_program.get_uniform_location(b"texcord_prev_mat\0".as_ptr()),
        u_texcord_curr_mat: gl_program.get_uniform_location(b"texcord_curr_mat\0".as_ptr()),
        u_tex_prev: gl_program.get_uniform_location(b"tex_prev\0".as_ptr()),
        u_tex_curr: gl_program.get_uniform_location(b"tex_curr\0".as_ptr()),
        u_prog: gl_program.get_uniform_location(b"prog\0".as_ptr()),
        u_has_prev: gl_program.get_uniform_location(b"has_prev\0".as_ptr()),
        u_has_curr: gl_program.get_uniform_location(b"has_curr\0".as_ptr()),
        u_stage_transform: gl_program.get_uniform_location(b"stageTransform\0".as_ptr()),
        gl_program,
        vert_array, vert_buf
      })
    }
  }

  pub fn texcord_matrix(x: f32, y: f32, w: f32, h: f32) -> [f32; 9] {
    // Dot[{{1/w,0,0},{0,1/h,0},{0,0,1}} ,{{1,0,-x},{0,1,-y},{0,0,1}}] //MatrixForm
    [1f32 / w, 0f32, 0f32,  0f32, 1f32/h, 0f32,  -x/w, -y/h, 1f32]
  }

  pub fn get_prog(&self, now: &time::Instant) -> f32 {
    if let Some(tr) = self.transition {
      if tr.0 > *now {
        return 0.0;
      }
      let t = now.duration_since(tr.0);
      let prog = t.div_duration_f32(tr.1);
      if prog > 1.0 {
        1.0
      } else {
        prog
      }
    } else {
      1.0
    }
  }

  pub fn start_transition(&mut self, now: time::Instant, duration: time::Duration) {
    self.transition = Some((now, duration));
  }
  pub fn stop(&mut self) {
    self.transition = None;
  }

  pub fn draw_box<T: std::borrow::Borrow<Texture>>(&mut self, draw_ctx: &mut DrawContext, prev_data: Option<DrawData<T>>, curr_data: Option<DrawData<T>>) {
    if prev_data.is_none() && curr_data.is_none() {
      return;
    }
    self.gl_program.use_program();
    unsafe {
      gl::BindVertexArray(self.vert_array);
      gl::UniformMatrix3fv(self.u_stage_transform, 1, 0, draw_ctx.stage_transform_matrix.as_ptr());
    }
    let prog = self.get_prog(&draw_ctx.now);
    let mut overall_bounding_box: Option<(f32, f32, f32, f32)> = None;
    if let Some(ref prev_data) = prev_data {
      overall_bounding_box = Some(prev_data.bounding_box);
      let (x, y, w, h) = prev_data.bounding_box;
      let texcord_mat = Self::texcord_matrix(x, y, w, h);
      unsafe {
        gl::UniformMatrix3fv(self.u_texcord_prev_mat, 1, 0, texcord_mat.as_ptr());
        gl::ActiveTexture(gl::TEXTURE0);
        prev_data.texture.borrow().bind();
        gl::Uniform1i(self.u_tex_prev, 0);
        gl::Uniform1i(self.u_has_prev, 1);
      }
    } else {
      unsafe {
        gl::Uniform1i(self.u_has_prev, 0);
      }
    }
    if let Some(ref curr_data) = curr_data {
      let (mut x, mut y, w, h) = curr_data.bounding_box;
      let flow_prog = 1.0 - f32::powf(prog, 0.5);
      match self.flow_effect {
        FlowEffect::NONE => {},
        FlowEffect::RIGHT => {
          x -= flow_prog * self.flow_effect_offset
        },
        FlowEffect::LEFT => {
          x += flow_prog * self.flow_effect_offset
        },
        FlowEffect::UP => {
          y += flow_prog * self.flow_effect_offset
        },
        FlowEffect::DOWN => {
          y -= flow_prog * self.flow_effect_offset
        }
      };
      if let Some((px, py, pw, ph)) = overall_bounding_box {
        let mut new_box = (
          f32::min(px, x),
          f32::min(py, y),
          f32::max(x + w, px + pw),
          f32::max(y + h, py + ph)
        );
        new_box.2 -= new_box.0;
        new_box.3 -= new_box.1;
        overall_bounding_box = Some(new_box);
      } else {
        overall_bounding_box = Some((x, y, w, h));
      }
      let texcord_mat = Self::texcord_matrix(x, y, w, h);
      unsafe {
        gl::UniformMatrix3fv(self.u_texcord_curr_mat, 1, 0, texcord_mat.as_ptr());
        gl::ActiveTexture(gl::TEXTURE1);
        curr_data.texture.borrow().bind();
        gl::Uniform1i(self.u_tex_curr, 1);
        gl::Uniform1i(self.u_has_curr, 1);
      }
    } else {
      unsafe {
        gl::Uniform1i(self.u_has_curr, 0);
      }
    }
    unsafe {
      gl::Uniform1f(self.u_prog, prog);
      gl::Enable(gl::BLEND);
      gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);
      let (x, y, w, h) = overall_bounding_box.unwrap();
      let verts = box_verticies(x, y, w, h);
      gl::BindBuffer(gl::ARRAY_BUFFER, self.vert_buf);
      gl::BufferData(gl::ARRAY_BUFFER, mem::size_of_val(&verts[..]) as isize, verts.as_ptr() as *const _, gl::DYNAMIC_DRAW);
			gl::DrawArrays(gl::TRIANGLE_STRIP, 0, 4);
    }
  }
}
