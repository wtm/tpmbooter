#version 330 core
in vec2 coordinate;
in vec2 texcord_in;
out vec2 texcord;
uniform mat3 stageTransform;

void main () {
  gl_Position = vec4(stageTransform * vec3(coordinate, 1.0), 1.0);
  texcord = texcord_in;
}
