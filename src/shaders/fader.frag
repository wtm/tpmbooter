#version 330 core
in vec2 texcord_prev_out;
in vec2 texcord_curr_out;
out vec4 FragColor;
uniform sampler2D tex_prev;
uniform sampler2D tex_curr;
uniform float prog;
uniform bool has_prev;
uniform bool has_curr;

bool inRange(vec2 pos) {
  if ((pos.x < 0.0) || (pos.x >= 1.0)) {
    return false;
  }
  if ((pos.y < 0.0) || (pos.y >= 1.0)) {
    return false;
  }
  return true;
}

void main () {
  bool show_prev = has_prev && inRange(texcord_prev_out);
  bool show_curr = has_curr && inRange(texcord_curr_out);
  if (show_prev && show_curr) {
    FragColor = prog * texture(tex_curr, texcord_curr_out) + (1.0 - prog) * texture(tex_prev, texcord_prev_out);
  } else if (show_curr) {
    FragColor = prog * texture(tex_curr, texcord_curr_out);
  } else if (show_prev) {
    FragColor = (1.0 - prog) * texture(tex_prev, texcord_prev_out);
  } else {
    FragColor = vec4(0.0, 0.0, 0.0, 0.0);
  }
}
