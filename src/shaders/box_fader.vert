#version 330 core
in vec2 coordinate;
out vec2 texcord_prev_out;
out vec2 texcord_curr_out;
uniform mat3 stageTransform;
uniform mat3 texcord_prev_mat;
uniform mat3 texcord_curr_mat;

void main () {
  gl_Position = vec4(stageTransform * vec3(coordinate, 1.0), 1.0);
  texcord_prev_out = (texcord_prev_mat * vec3(coordinate, 1.0)).xy;
  texcord_curr_out = (texcord_curr_mat * vec3(coordinate, 1.0)).xy;
}
