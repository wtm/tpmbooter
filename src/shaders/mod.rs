use gl;

pub struct Shader {
  shader_id: u32,
}

macro_rules! load_shader {
  ($filename:expr, $type:expr) => {{
    let shader_content = include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/src/shaders/", $filename));
    unsafe {
      let shader = gl::CreateShader($type);
      if shader == 0 {
        let glerror = gl::GetError();
        Err(format!("{}: glCreateShader got 0 with error 0x{:x}.", $filename, glerror))
      } else {
        gl::ShaderSource(shader, 1, [shader_content.as_ptr() as *const i8].as_ptr(), [shader_content.len() as i32].as_ptr());
        gl::CompileShader(shader);
        let mut shader_status = 0;
        gl::GetShaderiv(shader, gl::COMPILE_STATUS, &mut shader_status);
        let mut info_log_buf: Vec<u8> = vec![0; 1<<15];
        let mut ret_length = info_log_buf.len() as i32;
        gl::GetShaderInfoLog(shader, info_log_buf.len() as i32, &mut ret_length, info_log_buf.as_mut_ptr() as *mut i8);
        info_log_buf.truncate(ret_length as usize);
        if shader_status == 0 {
          gl::DeleteShader(shader);
          if ret_length == 0 {
            Err(format!("{}: unknow error", $filename))
          } else {
            Err(format!("{}: {}", $filename, String::from_utf8_lossy(&info_log_buf)))
          }
        } else {
          if ret_length > 0 {
            eprintln!("{}: compiler info log: {}", $filename, String::from_utf8_lossy(&info_log_buf));
          }
          Ok(crate::shaders::Shader::from_gl_shader(shader))
        }
      }
    }
  }};
}

impl Shader {
  pub unsafe fn from_gl_shader(shader: u32) -> Self {
    Self{
      shader_id: shader
    }
  }
}

impl Drop for Shader {
  fn drop(&mut self) {
    unsafe { gl::DeleteShader(self.shader_id) };
  }
}

pub struct Program {
  id: u32,
}

impl Program {
  pub fn link_shaders(shaders: &[Shader]) -> Result<Self, String> {
    unsafe {
      let prog_id = gl::CreateProgram();
      for shader in shaders {
        gl::AttachShader(prog_id, shader.shader_id);
      }
      gl::LinkProgram(prog_id);
      let mut success = 0;
      gl::GetProgramiv(prog_id, gl::LINK_STATUS, &mut success);
      let mut info_log_buf: Vec<u8> = vec![0; 1<<15];
      let mut ret_length = 0;
      gl::GetProgramInfoLog(prog_id, info_log_buf.len() as i32, &mut ret_length, info_log_buf.as_mut_ptr() as *mut i8);
      info_log_buf.truncate(ret_length as usize);
      if success == 0 {
        gl::DeleteProgram(prog_id);
        if ret_length == 0 {
          Err(String::from("unknow error"))
        } else {
          Err(String::from_utf8_lossy(&info_log_buf).into_owned())
        }
      } else {
        if ret_length > 0 {
          eprintln!("linker info log: {}", String::from_utf8_lossy(&info_log_buf));
        }
        Ok(Program{id: prog_id})
      }
    }
  }

  pub fn use_program(&self) {
    unsafe { gl::UseProgram(self.id) };
  }

  pub unsafe fn get_uniform_location(&self, name: *const u8) -> i32 {
    gl::GetUniformLocation(self.id, name as *const _)
  }

  pub unsafe fn get_attr_location(&self, name: *const u8) -> u32 {
    let attrloc: i32 = gl::GetAttribLocation(self.id, name as *const _);
    if attrloc == -1 {
      eprintln!("vertex attr {} not found.", std::ffi::CStr::from_ptr(name as *const _).to_string_lossy());
      panic!("!")
    } else {
      attrloc as u32
    }
  }
}

impl Drop for Program {
  fn drop(&mut self) {
  }
}
