use libc;
use std::{io, mem};
use std::io::Write;

#[derive(Default)]
pub struct TermiosStateChange {
	isatty: bool,
	init_termios: Option<libc::termios>,
	cursor_hidden: bool,
}

impl TermiosStateChange {
	pub fn init() -> Self {
		let mut this = Self::default();
		if unsafe { libc::isatty(0) } == 1 {
			this.isatty = true;
			unsafe {
				let mut termios: libc::termios = mem::zeroed();
				if libc::tcgetattr(0, &mut termios) == 0 {
					this.init_termios = Some(termios);
				}
			}
		}
		this
	}

	pub fn put_raw(&mut self) {
		if !self.isatty { return }
		if let Some(ref init_termios) = self.init_termios {
			let mut new_termios = *init_termios;
			unsafe {
				new_termios.c_iflag &= !(libc::IGNBRK | libc::BRKINT | libc::PARMRK | libc::ISTRIP | libc::INLCR | libc::IGNCR | libc::ICRNL | libc::IXON);
				new_termios.c_oflag &= !libc::OPOST;
				new_termios.c_lflag &= !(libc::ECHO | libc::ECHONL | libc::ICANON | libc::ISIG | libc::IEXTEN);
				new_termios.c_cflag |= libc::CS8;
				libc::tcsetattr(0, libc::TCSADRAIN, &new_termios);
			}
		}
		if io::stderr().write_all(b"\x1b[?25l").is_ok() {
			self.cursor_hidden = true;
		}
	}
}

impl Drop for TermiosStateChange {
	fn drop(&mut self) {
		if !self.isatty { return }
		if let Some(init_termios) = self.init_termios {
			unsafe {
				libc::tcsetattr(0, libc::TCSADRAIN, &init_termios);
			}
		}
		if self.cursor_hidden {
			let _ = io::stderr().write_all(b"\x1b[?25h");
		}
	}
}
