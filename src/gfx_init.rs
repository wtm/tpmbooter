use std::{mem, ffi, ptr};
use gl;
use tpmbooter_ffis::{egl, libdrm, gbm};

#[derive(Default)]
pub struct GlIniter {
	dri_fd: Option<libc::c_int>,
	drm_has_master: bool,
	gbm: Option<*mut gbm::gbm_device>,
	egl_display: Option<egl::EGLDisplay>,
	drm_mode_resources: Option<libdrm::drmModeResPtr>,
	connector: Option<&'static mut libdrm::drmModeConnector>,
	encoder: Option<&'static mut libdrm::drmModeEncoder>,
	egl_ctx: Option<egl::EGLContext>,
	gbm_surface: Option<*mut gbm::gbm_surface>,
	egl_surface: Option<egl::EGLSurface>,
	drm_crtc: Option<libdrm::drmModeCrtcPtr>,

	pub width: i32,
	pub height: i32,

	need_restore_crtc: bool,
	prev_bo: Option<*mut gbm::gbm_bo>,
	prev_fb: Option<u32>,
}

impl GlIniter {
	pub fn init() -> Result<Self, String> {
		macro_rules! fail {
			($this:ident, $($fmt:expr),*) => {
				let err_str = format!($($fmt),*);
				mem::drop($this);
        return Err(err_str);
			};
		}
		unsafe {
			let mut this = GlIniter::default();
			if egl::eglBindAPI(egl::EGL_OPENGL_API) == egl::EGL_FALSE {
				fail!(this, "eglBindAPI failed.");
			}

			let ptr_errno = libc::__errno_location();
			let fd = libc::open("/dev/dri/card0\0".as_ptr() as *const _, libc::O_RDWR);
			if fd < 0 {
				fail!(this, "Error opening card0: {}", ffi::CStr::from_ptr(libc::strerror(*ptr_errno)).to_string_lossy());
			}
			this.dri_fd = Some(fd);
			if libdrm::drmSetMaster(fd) < 0 {
				// fail!(this, "drmSetMaster failed.");
				eprintln!("Warning: unable to drmSetMaster, ignoring.");
			} else {
				this.drm_has_master = true;
			}
			let gbm_device = gbm::gbm_create_device(fd);
			if gbm_device.is_null() {
				fail!(this, "gbm_create_device failed.");
			}
			this.gbm = Some(gbm_device);
			let egl_display = egl::eglGetDisplay(gbm_device as *mut _);
			// /usr/include/EGL/egl.h:99:#define EGL_NO_DISPLAY                    EGL_CAST(EGLDisplay,0)
			if egl_display.is_null() {
				fail!(this, "eglGetDisplay failed.");
			}
			this.egl_display = Some(egl_display);
			let mut major: egl::EGLint = 0;
			let mut minor: egl::EGLint = 0;
			if egl::eglInitialize(egl_display, &mut major, &mut minor) != egl::EGL_TRUE {
				fail!(this, "eglInitialize failed with error 0x{:x}.", egl::eglGetError());
			}
			let drm_mode_res = libdrm::drmModeGetResources(fd);
			if drm_mode_res.is_null() {
				fail!(this, "drmModeGetResources returned null.");
			}
			this.drm_mode_resources = Some(drm_mode_res);
			let drm_mode_res = &*drm_mode_res;
			let mut connector_to_use = None;
			eprintln!("num_connectors = {}", drm_mode_res.count_connectors);
			for i in 0..drm_mode_res.count_connectors {
				let connector = libdrm::drmModeGetConnector(fd, *(drm_mode_res.connectors.add(i as usize)));
				if connector.is_null() {
					continue
				}
				let connector = &mut *connector;
				if connector.connection == libdrm::drmModeConnection_DRM_MODE_CONNECTED && connector.count_modes > 0 {
					connector_to_use = Some(connector);
					break
				} else {
					libdrm::drmModeFreeConnector(connector);
				}
			}
			if connector_to_use.is_none() {
				fail!(this, "No connected connectors!");
			}
			this.connector = connector_to_use;
			let connector = this.connector.as_ref().unwrap();

			let mut encoder_to_use = None;
			for i in 0..drm_mode_res.count_encoders {
				let encoder = libdrm::drmModeGetEncoder(fd, *(drm_mode_res.encoders.add(i as usize)));
				if encoder.is_null() {
					continue
				}
				let encoder = &mut *encoder;
				if encoder.encoder_id == connector.encoder_id {
					encoder_to_use = Some(encoder);
					break
				} else {
					libdrm::drmModeFreeEncoder(encoder);
				}
			}
			if encoder_to_use.is_none() {
				fail!(this, "No encoders!");
			}
			this.encoder = encoder_to_use;
			let encoder = this.encoder.as_ref().unwrap();

			// connector->count_modes checked before to be >0.
			let mode = &*connector.modes;

			let crtc = libdrm::drmModeGetCrtc(fd, encoder.crtc_id);
			if crtc.is_null() {
				fail!(this, "No crtc.");
			}
			this.drm_crtc = Some(crtc);

			const NULLPTR: *const i8 = 0 as *const i8;
			match egl::eglQueryString(egl_display, egl::EGL_VERSION as i32) {
				NULLPTR => { fail!(this, "eglQueryString(..., EGL_VERSION) got null."); },
				version => println!("EGL_VERSION = {}", ffi::CStr::from_ptr(version).to_string_lossy())
			};
			match egl::eglQueryString(egl_display, egl::EGL_EXTENSIONS as i32) {
				NULLPTR => { fail!(this, "eglQueryString(..., EGL_EXTENSIONS) got null."); },
				exts => {
					let exts = ffi::CStr::from_ptr(exts).to_string_lossy();
					if exts.find("EGL_KHR_create_context").is_none() {
						println!("EGL_EXTENSIONS = {}", exts);
						fail!(this, "EGL_KHR_create_context extension required.");
					}
				}
			};

			let mut target_egl_config: egl::EGLConfig = mem::zeroed();
			let mut num_config: egl::EGLint = mem::zeroed();
			if egl::eglChooseConfig(egl_display, [
				egl::EGL_SURFACE_TYPE as i32, egl::EGL_WINDOW_BIT as i32,
				egl::EGL_RED_SIZE as i32, 8,
				egl::EGL_GREEN_SIZE as i32, 8,
				egl::EGL_BLUE_SIZE as i32, 8,
				egl::EGL_ALPHA_SIZE as i32, 8,
				egl::EGL_RENDERABLE_TYPE as i32, egl::EGL_OPENGL_BIT as i32,
				egl::EGL_NONE as i32
			].as_ptr(), &mut target_egl_config, 1, &mut num_config) != egl::EGL_TRUE {
				fail!(this, "eglChooseConfig failed.");
			}
			if num_config == 0 {
				fail!(this, "No opengl config.");
			}

			// /usr/include/EGL/egl.h:98:#define EGL_NO_CONTEXT                    EGL_CAST(EGLContext,0)
			let eglctx = egl::eglCreateContext(egl_display, target_egl_config, ptr::null_mut(), [egl::EGL_CONTEXT_MAJOR_VERSION_KHR as i32, 3, egl::EGL_CONTEXT_MINOR_VERSION_KHR as i32, 3, egl::EGL_CONTEXT_OPENGL_PROFILE_MASK_KHR as i32, egl::EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT as i32, egl::EGL_NONE as i32].as_ptr());
			if eglctx.is_null() {
				fail!(this, "eglCreateContext returned null with error 0x{:x}.", egl::eglGetError());
			}
			this.egl_ctx = Some(eglctx);

			this.width = i32::from(mode.hdisplay);
			this.height = i32::from(mode.vdisplay);
			eprintln!("width = {}, height = {}", this.width, this.height);
			let gbm_surf = gbm::gbm_surface_create(gbm_device, u32::from(mode.hdisplay), u32::from(mode.vdisplay), gbm::gbm_bo_format_GBM_BO_FORMAT_ARGB8888, gbm::gbm_bo_flags_GBM_BO_USE_SCANOUT | gbm::gbm_bo_flags_GBM_BO_USE_RENDERING);
			if gbm_surf.is_null() {
				fail!(this, "gbm_surface_create -> null");
			}
			this.gbm_surface = Some(gbm_surf);

			let egl_surf = egl::eglCreateWindowSurface(egl_display, target_egl_config, gbm_surf as _, ptr::null());
			if egl_surf.is_null() {
				fail!(this, "eglCreateWindowSurface failed with error 0x{:x}.", egl::eglGetError());
			}
			this.egl_surface = Some(egl_surf);

			if egl::eglMakeCurrent(egl_display, egl_surf, egl_surf, eglctx) != egl::EGL_TRUE {
				fail!(this, "eglMakeCurrent failed.");
			}

			let mut ctx_version = mem::zeroed();
			match egl::eglQueryContext(egl_display, eglctx, egl::EGL_CONTEXT_CLIENT_VERSION as i32, &mut ctx_version) {
				0 => { fail!(this, "eglQueryContext(..., EGL_CONTEXT_CLIENT_VERSION) errored."); },
				_ => println!("EGL_CONTEXT_CLIENT_VERSION = {}", ctx_version)
			};

			gl::load_with(|s| {
				let name = ffi::CString::new(s.as_bytes()).unwrap();
				mem::transmute(egl::eglGetProcAddress(name.as_ptr()))
			});

			Ok(this)
		}
	}

	pub fn next_frame(&mut self) -> Result<(), &'static str> {
		unsafe {
			if self.egl_display.is_none() || self.egl_surface.is_none() || self.gbm_surface.is_none() || self.dri_fd.is_none() || self.encoder.is_none() {
				return Err("Object not properly initialized.");
			}
			let dri_fd = self.dri_fd.unwrap();
			if egl::eglSwapBuffers(self.egl_display.unwrap(), self.egl_surface.unwrap()) != egl::EGL_TRUE {
				return Err("eglSwapBuffers failed.");
			}
			let next_bo = gbm::gbm_surface_lock_front_buffer(self.gbm_surface.unwrap());
			if next_bo.is_null() {
				return Err("gbm_surface_lock_front_buffer failed.");
			}
			let mut fb_id = mem::uninitialized();
			if libdrm::drmModeAddFB(dri_fd, self.width as _, self.height as _, 24, 32, gbm::gbm_bo_get_stride(next_bo), gbm::gbm_bo_get_handle(next_bo).u32, &mut fb_id) != 0 {
				return Err("drmModeAddFB failed.");
			}
			let ptr_errno = libc::__errno_location();
			if !self.need_restore_crtc {
				if libdrm::drmModeSetCrtc(dri_fd, self.encoder.as_ref().unwrap().crtc_id, fb_id, 0, 0, [self.connector.as_ref().unwrap().connector_id].as_mut_ptr(), 1, self.connector.as_ref().unwrap().modes) != 0 {
					return Err("drmModeSetCrtc failed.");
				}
				self.need_restore_crtc = true;
			} else if libdrm::drmModePageFlip(dri_fd, self.encoder.as_ref().unwrap().crtc_id, fb_id, libdrm::DRM_MODE_PAGE_FLIP_EVENT, self as *mut _ as *mut _) != 0 {
				let mut vbl: libdrm::drmVBlank = mem::zeroed();
				vbl.request.type_ = libdrm::drm_vblank_seq_type__DRM_VBLANK_RELATIVE;
				vbl.request.sequence = 1;
				libdrm::drmWaitVBlank(dri_fd, &mut vbl);
				if libdrm::drmModeSetCrtc(dri_fd, self.encoder.as_ref().unwrap().crtc_id, fb_id, 0, 0, [self.connector.as_ref().unwrap().connector_id].as_mut_ptr(), 1, self.connector.as_ref().unwrap().modes) != 0 {
					let errno = *ptr_errno;
					libdrm::drmModeRmFB(dri_fd, fb_id);
					gbm::gbm_surface_release_buffer(self.gbm_surface.unwrap(), next_bo);
					return Err(Box::leak(Box::new(String::from("drmModeSetCrtc failed: ") + &ffi::CStr::from_ptr(libc::strerror(errno)).to_string_lossy())));
				}
			} else {
				let mut evctx: libdrm::drmEventContext = mem::zeroed();
				evctx.version = libdrm::DRM_EVENT_CONTEXT_VERSION as _;
				evctx.page_flip_handler = Some(Self::drm_page_flip_event_handler);
				let mut pollfds: [libc::pollfd; 1] = [mem::zeroed()];
				pollfds[0].fd = dri_fd;
				pollfds[0].events = libc::POLLIN;
				libc::poll(pollfds.as_mut_ptr(), pollfds.len() as _, 1000);
				libdrm::drmHandleEvent(dri_fd, &mut evctx);
			}
			if let Some(fb) = self.prev_fb {
				libdrm::drmModeRmFB(dri_fd, fb);
			}
			self.prev_fb = Some(fb_id);
			if let Some(bo) = self.prev_bo {
				gbm::gbm_surface_release_buffer(self.gbm_surface.unwrap(), bo);
			}
			self.prev_bo = Some(next_bo);
			Ok(())
		}
	}

	unsafe extern "C" fn drm_page_flip_event_handler(_fd: std::os::raw::c_int, _sequence: std::os::raw::c_uint, _tv_sec: std::os::raw::c_uint, _tv_usec: std::os::raw::c_uint, _user_data: *mut std::os::raw::c_void) {
	}
}

impl Drop for GlIniter {
	fn drop(&mut self) {
		if let Some(gbm_surf) = self.gbm_surface {
			if let Some(bo) = self.prev_bo {
				unsafe { gbm::gbm_surface_release_buffer(gbm_surf, bo) };
			}
		}
		if let Some(dri_fd) = self.dri_fd {
			if let Some(fb) = self.prev_fb {
				unsafe { libdrm::drmModeRmFB(dri_fd, fb) };
			}
		}
		if let Some(crtc) = self.drm_crtc {
			if self.need_restore_crtc {
				unsafe {
					let crtc = &mut *crtc;
					libdrm::drmModeSetCrtc(self.dri_fd.unwrap(), crtc.crtc_id, crtc.buffer_id, crtc.x, crtc.y, [self.connector.as_ref().unwrap().connector_id].as_mut_ptr(), 1, &mut crtc.mode);
				}
			}
			unsafe { libdrm::drmModeFreeCrtc(crtc) };
		}
		if let Some(egl_display) = self.egl_display {
			if let Some(egl_surf) = self.egl_surface {
				unsafe { egl::eglDestroySurface(egl_display, egl_surf) };
				self.egl_surface = None;
			}
			if let Some(egl_ctx) = self.egl_ctx {
				unsafe { egl::eglDestroyContext(egl_display, egl_ctx) };
				self.egl_ctx = None;
			}
		}
		if let Some(gbmsurf) = self.gbm_surface {
			unsafe { gbm::gbm_surface_destroy(gbmsurf) };
			self.gbm_surface = None;
		}
		if let Some(ref mut encoder) = self.encoder {
			unsafe { libdrm::drmModeFreeEncoder(*encoder) };
			self.encoder = None;
		}
		if let Some(ref mut connector) = self.connector {
			unsafe { libdrm::drmModeFreeConnector(*connector) };
			self.connector = None;
		}
		if let Some(drm_res) = self.drm_mode_resources {
			unsafe { libdrm::drmModeFreeResources(drm_res) };
			self.drm_mode_resources = None;
		}
		if let Some(egl_display) = self.egl_display {
			unsafe { egl::eglTerminate(egl_display) };
			self.egl_display = None;
		}
		if let Some(gbm_device) = self.gbm {
			unsafe { gbm::gbm_device_destroy(gbm_device) };
			self.gbm = None;
		}
		if let Some(fd) = self.dri_fd {
			if self.drm_has_master {
				unsafe { libdrm::drmDropMaster(fd) };
			}
			unsafe { libc::close(fd) };
			self.dri_fd = None;
		}
	}
}
