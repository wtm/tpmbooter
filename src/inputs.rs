use std::pin::Pin;
use std::{mem, ffi, ptr};
use tpmbooter_ffis::{libudev, libinput};

pub struct LibinputCtx {
	ctx: *mut libinput::libinput,
	interface: *mut libinput::libinput_interface,
	pub cursor_x_norm: f64, // 0 to 1
	pub cursor_y_norm: f64, // 0 to 1
	pub __exit: bool,
}

impl LibinputCtx {
	pub fn init() -> Result<Pin<Box<Self>>, &'static str> {
		unsafe {
			let interface = Box::into_raw(Box::new(libinput::libinput_interface{
				close_restricted: Some(LibinputCtx::__close_restricted),
				open_restricted: Some(LibinputCtx::__open_restricted),
			}));
			let mut selfbox = Box::pin(LibinputCtx{ctx: ptr::null_mut(), interface, cursor_x_norm: 0.5, cursor_y_norm: 0.5, __exit: false});

			let udevctx = libudev::udev_new();
			if udevctx.is_null() {
				return Err("udev_new returned NULL.");
			}
			let ctx = libinput::libinput_udev_create_context(interface, &*selfbox as *const _ as *mut _, udevctx as *mut libinput::udev);
			libudev::udev_unref(udevctx);
			selfbox.as_mut().get_unchecked_mut().ctx = ctx;

			if libinput::libinput_udev_assign_seat(ctx, "seat0\0".as_ptr() as *const _) != 0 {
				return Err("libinput_udev_assign_seat errored.");
			}

			Ok(selfbox)
		}
	}

	unsafe extern "C" fn __open_restricted(path: *const std::os::raw::c_char, flags: std::os::raw::c_int, _user_data: *mut std::os::raw::c_void) -> std::os::raw::c_int {
		let ptr_errno = libc::__errno_location();
		let fd = libc::open(path, flags);
		if fd == -1 {
			eprintln!("Error opening: {}", ffi::CStr::from_ptr(libc::strerror(*ptr_errno)).to_string_lossy());
			-1
		} else {
			fd
		}
	}

	unsafe extern "C" fn __close_restricted(fd: std::os::raw::c_int, _user_data: *mut std::os::raw::c_void) {
		libc::close(fd);
	}

	pub fn dispatch_events(&mut self) {
		unsafe {
			if libinput::libinput_dispatch(self.ctx) != 0 {
				eprintln!("dispatch event failed.");
				return
			}
			loop {
				let next_event = libinput::libinput_get_event(self.ctx);
				if next_event.is_null() {
					return
				}
				self.handle_event(next_event);
				libinput::libinput_event_destroy(next_event);
			}
		}
	}

	unsafe fn handle_event(&mut self, evt: *mut libinput::libinput_event) {
		match libinput::libinput_event_get_type(evt) {
			libinput::libinput_event_type_LIBINPUT_EVENT_KEYBOARD_KEY => {
				self.__exit = true;
				let evt = libinput::libinput_event_get_keyboard_event(evt);
				println!("keyboard event: code = {}, state = {}", libinput::libinput_event_keyboard_get_key(evt), libinput::libinput_event_keyboard_get_key_state(evt));
			}
			libinput::libinput_event_type_LIBINPUT_EVENT_POINTER_MOTION_ABSOLUTE => {
				let evt = libinput::libinput_event_get_pointer_event(evt);
				let x = libinput::libinput_event_pointer_get_absolute_x_transformed(evt, 1);
				let y = libinput::libinput_event_pointer_get_absolute_y_transformed(evt, 1);
				self.cursor_x_norm = x;
				self.cursor_y_norm = y;
			}
			libinput::libinput_event_type_LIBINPUT_EVENT_POINTER_MOTION => {
				let evt = libinput::libinput_event_get_pointer_event(evt);
				let dx = libinput::libinput_event_pointer_get_dx(evt);
				let dy = libinput::libinput_event_pointer_get_dy(evt);
				self.cursor_x_norm += dx / 1920.0;
				self.cursor_y_norm += dy / 1080.0;
			}
			libinput::libinput_event_type_LIBINPUT_EVENT_TOUCH_MOTION => {
				let evt = libinput::libinput_event_get_touch_event(evt);
				let x = libinput::libinput_event_touch_get_x_transformed(evt, 1);
				let y = libinput::libinput_event_touch_get_y_transformed(evt, 1);
				self.cursor_x_norm = x;
				self.cursor_y_norm = y;
			}
			_ => {}
		}
		if self.cursor_x_norm < 0.0 {
			self.cursor_x_norm = 0.0;
		}
		if self.cursor_x_norm > 1.0 {
			self.cursor_x_norm = 1.0;
		}
		if self.cursor_y_norm < 0.0 {
			self.cursor_y_norm = 0.0;
		}
		if self.cursor_y_norm > 1.0 {
			self.cursor_y_norm = 1.0;
		}
	}
}

impl Drop for LibinputCtx {
	fn drop(&mut self) {
		unsafe {
			libinput::libinput_unref(self.ctx);
			mem::drop(Box::from_raw(self.interface));
		}
	}
}
