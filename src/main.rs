#![feature(duration_float)]
#![feature(duration_constants)]
use std::{mem, io, time, ffi, ptr};
use libc;
use std::process;
use std::io::{Read, Write};
use rand::{prelude::Rng, SeedableRng};

use gl;
mod gfx_init;
mod inputs;
mod termios;

#[macro_use]
mod shaders;
mod gfx_components;
use gfx_components::cursor::Cursor;
use gfx_components::Drawable;
use gfx_components::{simple_image, text, transitions, logo::Logo};

fn main () {
	use std::fs;
	if let Err(e) = fs::metadata("/init") {
		if e.kind() == io::ErrorKind::NotFound {
			eprintln!("Not running in normal linux.");
			eprintln!("To debug or test this program, run \"make qemu-vm\".")
		} else {
			eprintln!("Error stating /init: {}", &e);
		}
		std::process::exit(1);
	}
	if !init() {
		eprintln!("Droping you to a shell:");
		let _ = std::io::stderr().flush();
		let _ = std::io::stdout().flush();
		unsafe {
			let ptr_errno = libc::__errno_location();
			let v = ["/bin/ash\0".as_ptr() as *const i8, ptr::null()];
			libc::execv("/bin/ash\0".as_ptr() as *const i8, v.as_ptr());
			eprint!("execv error: {}\n", io::Error::from_raw_os_error(*ptr_errno));
			process::exit(1)
		}
	}
}

fn print_uname () {
	unsafe {
		let ptr_errno = libc::__errno_location();
		let mut s_uname: libc::utsname = mem::zeroed();
		if libc::uname(&mut s_uname) == 0 {
			let w = |x: &[i8]| ffi::CStr::from_ptr(x as *const [i8] as *const i8).to_str().unwrap();
			println!("{}: {} {}", w(&s_uname.sysname), w(&s_uname.release), w(&s_uname.version));
		} else {
			eprintln!("Unable to get uname: {}", io::Error::from_raw_os_error(*ptr_errno));
		}
	}
}

fn init() -> bool {
	std::env::set_var("RUST_BACKTRACE", "1");
	std::env::set_current_dir("/").unwrap();
	print_uname();

	let mut iptctx = match inputs::LibinputCtx::init() {
		Ok(i) => i,
		Err(e) => {
			eprintln!("Error opening inputs: {}", &e);
			return false;
		}
	};

	let mut gfx = match gfx_init::GlIniter::init() {
		Ok(gfx) => gfx,
		Err(e) => {
			eprintln!("Error initializing graphics: {}", &e);
			return false;
		}
	};

	let mut _term_state_change = termios::TermiosStateChange::init();
	if std::env::var("TERM").unwrap_or_else(|_| String::from("_")) == "linux" {
		_term_state_change.put_raw();
	}

	let stage_transform_matrix = [2.0f32 / (gfx.width as f32), 0.0, 0.0, 0.0, -2.0f32 / (gfx.height as f32), 0.0, -1.0, 1.0, 1.0];

	let init_ctx = gfx_components::InitContext{
		gfx: &gfx,
		stage_transform_matrix: &stage_transform_matrix[..],
		now: time::Instant::now(),
	};

	let mut cursor = match Cursor::init(&init_ctx) {
		Ok(c) => c,
		Err(e) => {mem::drop(_term_state_change); eprintln!("{}", &e); return false;}
	};

	let opensans = rusttype::FontCollection::from_bytes(&include_bytes!("gfx_components/OpenSans-Regular.ttf")[..]).unwrap().into_fonts().next().unwrap().unwrap();
	let hack = rusttype::FontCollection::from_bytes(&include_bytes!("gfx_components/Hack-Regular.ttf")[..]).unwrap().into_fonts().next().unwrap().unwrap();

	let mut logo = Logo::init(&init_ctx).unwrap();

	let mut fps_text = text::TextRenderer::init(hack.clone());
	fps_text.set_scale(18f32);
	let mut fps_meter = text::FadeText::init(&init_ctx, fps_text, gfx.width as f32, gfx.height as f32 - 5f32).unwrap();
	fps_meter.align = text::Align::RIGHT;

	let mut cmdline = text::FadeText::init(&init_ctx, {
		let mut t = text::TextRenderer::init(hack.clone());
		t.set_scale(20f32);
		t
	}, (gfx.width / 2) as f32, (gfx.height * 7 / 10) as f32).unwrap();
	cmdline.align = text::Align::CENTER;
	cmdline.flow_effect = transitions::FlowEffect::RIGHT;
	cmdline.oversampling_factor = 2;
	{
		let mut cmdline_file = std::fs::File::open("/proc/cmdline").unwrap();
		let mut cmdline_str = String::new();
		cmdline_file.read_to_string(&mut cmdline_str).unwrap();
		cmdline.update(cmdline_str.trim(), time::Instant::now());
	}

	let mut press_any_key = text::FadeText::init(&init_ctx, {
		let mut t = text::TextRenderer::init(opensans.clone());
		t.set_scale(26f32);
		t
	}, (gfx.width / 2) as f32, (gfx.height * 8 / 10) as f32).unwrap();
	press_any_key.align = text::Align::CENTER;
	press_any_key.flow_effect = transitions::FlowEffect::RIGHT;
	press_any_key.update("Press any key to continue.", time::Instant::now() + time::Duration::from_millis(500));

	mem::drop(init_ctx);

	let mut trnd = rand::rngs::SmallRng::seed_from_u64(time::SystemTime::now().duration_since(time::SystemTime::UNIX_EPOCH).unwrap().as_secs());
	let mut curr_color: (f32, f32, f32) = (0.0, 0.0, 0.0);

	fn gen_next_color<R: Rng>(trnd: &mut R) -> (f32, f32, f32) {
		(trnd.gen::<f32>() / 4.0f32, trnd.gen::<f32>() / 4.0f32, trnd.gen::<f32>() / 4.0f32)
	}

	// let mut next_color: (f32, f32, f32) = gen_next_color(&mut trnd);
	let mut next_color: (f32, f32, f32) = (0.0, 0.0, 0.0);
	let mut last_color_switch = time::Instant::now();

	let mut last_fps_measurement_time = time::Instant::now();
	let mut frames_since_last_fps_measurement = 0u32;

	loop {
		let now = time::Instant::now();
		iptctx.dispatch_events();
		if iptctx.__exit {
			return true;
		}

		unsafe {
			gl::Viewport(0, 0, gfx.width, gfx.height);
			let prog = (now - last_color_switch).as_millis() as f32 / 5000.0;
			macro_rules! introp {
				($num:tt) => {
					curr_color.$num * (1.0 - prog) + prog * next_color.$num
				};
			}
			gl::ClearColor(introp!(0), introp!(1), introp!(2), 1.0);
			gl::Clear(gl::COLOR_BUFFER_BIT);
			if prog >= 1.0 {
				curr_color = next_color;
				next_color = gen_next_color(&mut trnd);
				last_color_switch = now;
			}
			let mut draw_ctx = gfx_components::DrawContext{
				iptctx: &mut iptctx,
				gfx: &gfx,
				now,
				stage_transform_matrix: &stage_transform_matrix[..]
			};
			fps_meter.draw(&mut draw_ctx);
			logo.draw(&mut draw_ctx);
			cmdline.draw(&mut draw_ctx);
			press_any_key.draw(&mut draw_ctx);
			cursor.draw(&mut draw_ctx);
		}
		unsafe {
			let mut glerr = gl::GetError();
			while glerr != gl::NO_ERROR {
				eprintln!("warning: gl error: {0} (0x{0:x})", glerr);
				glerr = gl::GetError();
			}
		}
		if let Err(e) = gfx.next_frame() {
			eprintln!("next_frame error: {}", e);
			return false;
		}
		let fpsdur = now.duration_since(last_fps_measurement_time);
		if fpsdur >= time::Duration::SECOND {
			let fps = frames_since_last_fps_measurement as f32 / fpsdur.as_secs_f32();
			fps_meter.update(&format!("fps ={: >5.1}", fps), now);
			last_fps_measurement_time = now;
			frames_since_last_fps_measurement = 0;
		} else {
			frames_since_last_fps_measurement += 1;
		}
	}
}
