#![allow(warnings)]

pub mod libinput {
  include!(concat!(env!("OUT_DIR"), "/bindings.libinput.rs"));
}
pub mod libudev {
  include!(concat!(env!("OUT_DIR"), "/bindings.libudev.rs"));
}
pub mod egl {
  include!(concat!(env!("OUT_DIR"), "/bindings.egl.rs"));
}
pub mod gbm {
  include!(concat!(env!("OUT_DIR"), "/bindings.gbm.rs"));
}
pub mod libdrm {
  include!(concat!(env!("OUT_DIR"), "/bindings.libdrm.rs"));
}
