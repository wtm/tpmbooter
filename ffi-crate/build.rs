use bindgen;
use std::env;
use pkg_config;

fn add_lib(name: &str, include_statements: &str) -> pkg_config::Library {
  let out_path = std::path::PathBuf::from(env::var("OUT_DIR").unwrap());

  let lib = pkg_config::Config::new().probe(name)
    .unwrap_or_else(|e| { eprintln!("pkg_config {} failed: {}", name, e); std::process::exit(1) });

  bindgen::builder()
    .clang_args(lib.include_paths.iter()
      .map(|x| { let mut str = String::from("-I"); str.push_str(x.canonicalize().unwrap_or(x.clone()).to_str().unwrap()); str }))
    .header_contents("wrapper.h", include_statements)
    .generate()
    .unwrap_or_else(|_| { eprintln!("bindgen {} failed.", name); std::process::exit(1) })
    .write_to_file(out_path.join(format!("bindings.{}.rs", name))).unwrap();

  return lib;
}

fn main () {
  add_lib("libinput", "#include <libinput.h>");
  add_lib("libudev", "#include <libudev.h>");
  add_lib("egl", "#include <EGL/egl.h>\n#include <EGL/eglext.h>");
  add_lib("gbm", "#include <gbm.h>");
  add_lib("libdrm", "#include <xf86drm.h>\n#include <xf86drmMode.h>");
}
