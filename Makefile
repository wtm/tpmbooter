KERNEL_VMLINUZ ?= /boot/vmlinuz-linux
TARGET ?= debug
VM_ROOTFS ?= ./_rootfs.img
VM_CMDLINE ?=
# VM_ROOTFS=/tmp/hd.raw VM_CMDLINE="root=UUID=e3be8881-6392-48d7-b739-d7aca03f4c3a ro quiet"
.PHONY: build clean qemu-vm runbin iso

build: target/$(TARGET)/tpmbooter
clean:
	cargo clean
	rm -rf initrd-build initrd.img.gz initrd.img

target/$(TARGET)/tpmbooter: Cargo.toml $(wildcard src/**/* src/*) ffi-crate/build.rs ffi-crate/src/*.rs
	cargo build `if [ $(TARGET) = release ]; then echo -n --release; fi`

initrd.img: target/$(TARGET)/tpmbooter $(wildcard mkinitcpio/**/* mkinitcpio/*)
	export TPMBOOTER_BIN=`pwd`/target/$(TARGET)/tpmbooter && \
	cd mkinitcpio && \
	./mkinitcpio -g ../initrd.img

qemu-vm: initrd.img $(VM_ROOTFS)
	qemu-system-x86_64 -cpu host -accel kvm -machine q35,accel=kvm -smp cpus=1,cores=1,threads=1,sockets=1 \
			-boot order=c,menu=on -m 2000 -audiodev id=none,driver=none -kernel $(KERNEL_VMLINUZ) -sandbox on \
			-device virtio-vga,virgl=on -display sdl,gl=on -initrd initrd.img -bios /usr/share/ovmf/x64/OVMF_CODE.fd \
			-drive file=$(VM_ROOTFS),format=raw \
			-append "MESA_DEBUG=true $(VM_CMDLINE)"

runbin: build
	target/$(TARGET)/tpmbooter

iso: vmboot.iso
vmboot.iso: initrd.img
	if [ ! -e ./vmboot.iso ]; then dd if=/dev/zero of=vmboot.iso count=1000 bs=1048576; mkfs.vfat ./vmboot.iso; fi
	if [ ! -e vmboot.mount ]; then mkdir vmboot.mount; fi
	sudo mount -o uid=`id -u`,gid=`id -g`,rw ./vmboot.iso ./vmboot.mount
	cp $(KERNEL_VMLINUZ) ./vmboot.mount/vmlinuz
	cp initrd.img ./vmboot.mount
	echo '\vmlinuz initrd=\initrd.img MESA_DEBUG=true' > vmboot.mount/startup.nsh
	sudo umount ./vmboot.mount
	rmdir vmboot.mount
