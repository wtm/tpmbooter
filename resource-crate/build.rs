use buildtime_png;

fn main() {
  buildtime_png::Builder::new()
    .include_png("images/left_ptr.png", "LEFT_PTR")
    .include_png("images/archlinux.png", "ARCHLINUX")
    .emit_source_file().unwrap();
}
